import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {language} from '../../Config/Localization';
import {connect} from 'react-redux';
import Metrics from '../../Config/Metrics';
import CustomMapView from '../../Components/CommonComponents/CustomMapView';
import DrawerNavBar from '../../Components/CommonComponents/DrawerNavBar';
import {
  getCurrentLocation, getMoreNearByGyms,
} from '../../Actions/LoginAction';
import {getUserProfile} from '../../Actions/UserDetailAction';
import {showLoader, hideLoader} from '../../Actions/AppAction';

class LocationScreen extends Component {

  static navigationOptions = {
    header: (props) => (
      <DrawerNavBar navigation={props.navigation} title={language.en.spotter} />
    ),
  };

  componentWillMount = async () => {
    const {navigation,showProfileFetchLoader,UserDetailState,fetchUserProfile} = this.props;

    if (UserDetailState.profileData == null) {
      showProfileFetchLoader();
      try {
        const userData = await AsyncStorage.getItem('userData');

        if (userData != null) {
          fetchUserProfile(
            JSON.parse(userData),
            navigation
          );
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  componentDidMount() {
    const {LoginState,fetchCurrentLocation,hideProfileLoader} = this.props;

    AsyncStorage.setItem('initialStack', 'DrawerStack');
    if (LoginState.currentRegion == null) {
      fetchCurrentLocation();
    }
    hideProfileLoader();
  }

  renderOnMapReady() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <CustomMapView onMarkerClick={(marker) => navigation.navigate('ScheduleWorkoutScreen', {
          gymObject: marker,
        })} />
        <View style={styles.markerContainer}>
          <View style={styles.markerSubContainer}>
            <TouchableOpacity
              style={styles.marker}
            >
              <Text style={styles.locationText}>{language.en.locations}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.friendsTab}
            >
              <Text style={styles.friendsText}>{language.en.friends}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.recentTab}
            >
              <Text style={styles.recentText}>{language.en.recent}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return this.renderOnMapReady();
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  friendsTab: {
    alignItems: 'center',
    height: 45,
    justifyContent: 'center',
    width: Metrics.DEVICE_WIDTH * 0.25,
  },
  friendsText: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  locationText: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  marker: {
    alignItems: 'center',
    height: 45,
    justifyContent: 'center',
    width: Metrics.DEVICE_WIDTH * 0.25,
  },
  markerContainer: {
    alignSelf: 'center',
    bottom: Metrics.DEVICE_HEIGHT * 0.1,
    flexWrap: 'wrap',
    position: 'absolute',
  },
  markerSubContainer: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 45,
    flexDirection: 'row',
    height: 45,
    justifyContent: 'center',
    width: Metrics.DEVICE_WIDTH * 0.75,
  },
  recentTab: {
    alignItems: 'center',
    height: 45,
    justifyContent: 'center',
    width: Metrics.DEVICE_WIDTH * 0.25,
  },
  recentText: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});

function mapStateToProps(state) {
  return {
    ChatState: state.ChatReducer,
    LoginState: state.LoginReducer,
    UserDetailState: state.UserDetailReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchCurrentLocation: () => dispatch(getCurrentLocation()),
    fetchUserProfile: (userData, navigation) =>
      dispatch(getUserProfile(userData, navigation)),
    hideProfileLoader: () => dispatch(hideLoader()),
    loadMoreGyms: (pageToken ,lat, long) => dispatch(getMoreNearByGyms(pageToken ,lat, long)),
    showProfileFetchLoader: () => dispatch(showLoader()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationScreen);
