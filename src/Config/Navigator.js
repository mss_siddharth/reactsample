import React from 'react';
import {
  Animated,
  Easing,
} from 'react-native';
import {StackNavigator, DrawerNavigator} from 'react-navigation';
import LocationScreen from '../Containers/HomeScreens/LocationScreen';
import DrawerNavBar from '../Components/CommonComponents/DrawerNavBar';
import SignUpScreen from '../Containers/OnBoardingScreens/SignUpScreen';
import ForgotPasswordScreen from '../Containers/OnBoardingScreens/ForgotPasswordScreen';
import LoginScreen from '../Containers/OnBoardingScreens/LoginScreen';
import {language} from '../Config/Localization';
import Metrics from '../Config/Metrics';
import DrawerMenu from '../Components/CommonComponents/DrawerMenu';
import InboxScreen from '../Containers/HomeScreens/InboxScreen';
import HelpCenterScreen from '../Containers/HomeScreens/HelpCenterScreen';
import NotificationScreen from '../Containers/HomeScreens/NotificationScreen';
import ProfileScreen from '../Containers/HomeScreens/ProfileScreen';

const transitionConfig = () => {
  return {
    screenInterpolator: (sceneProps) => {
      const {layout, position, scene} = sceneProps;

      const thisSceneIndex = scene.index;
      const width = layout.initWidth;

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      });

      return {transform: [{translateX}]};
    },
    transitionSpec: {
      duration: 500,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
  };
};
// drawer stack
const DrawerStack = DrawerNavigator(
  {
    DrawerLocationScreen: {screen: LocationScreen},
    HelpCenterScreen: {screen: HelpCenterScreen},
    InboxScreen: {screen: InboxScreen},
    NotificationScreen: {screen: NotificationScreen},
    ProfileScreen: {screen: ProfileScreen},
  },
  {
    contentComponent: DrawerMenu,
    drawerWidth: Metrics.DEVICE_WIDTH * 0.7,
    transitionConfig: transitionConfig,
  }
);
const DrawerNavigation = StackNavigator(
  {
    AppDrawerStack: {screen: DrawerStack},
  },
  {
    headerMode: 'none',
    navigationOptions: ({navigation}) => ({
      header: <DrawerNavBar navigation={navigation} title={language.en.spotter} />,
    }),
  }
);
// login stack
const LoginStack = StackNavigator(
  {
    AddLoginScreen: {screen: LoginScreen},
    ForgotPasswordScreen: {screen: ForgotPasswordScreen},
    SignUpScreen: {screen: SignUpScreen},
  },
  {
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false,
    },
    transitionConfig: transitionConfig,
  }
);
const PrimaryNav = StackNavigator(
  {
    AppLoginStack: {screen: LoginStack},
    DrawerStack: {screen: DrawerNavigation},
  },
  {
    headerMode: 'screen',
    initialRouteName: 'AppLoginStack',
    navigationOptions: {
      drawerLockMode: 'locked-closed',
    },
    transitionConfig: transitionConfig,
  }
);
const prevGetStateForActionHomeStack = PrimaryNav.router.getStateForAction;

PrimaryNav.router.getStateForAction = (action, state) => {
  if (state && action.type === 'ReplaceCurrentScreen') {
    const routes = state.routes.slice(0, state.routes.length - 1);

    routes.push(action);
    return {
      ...state,
      index: routes.length - 1,
      routes,
    };
  }
  return prevGetStateForActionHomeStack(action, state);
};

export default PrimaryNav;
