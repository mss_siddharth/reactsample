import WebApi from '../Utilities/RestClient';
import * as type from '../ActionTypes';
import {showLoader, hideLoader} from './AppAction';
export const onSignUp = (userData) => {
  return (dispatch) => {
    dispatch(showLoader());
    WebApi.PostApiHit('api/users/add.json', userData).then((signUpResponse) => {
      if (signUpResponse.success) {
        dispatch({signUpResponse: signUpResponse, type: type.SignUp_Success});
        dispatch(hideLoader());
      } else {
        dispatch({
          errorMessage: signUpResponse.data.message,
          type: type.SignUp_Failure,
        });
        dispatch(hideLoader());
      }
    });
  };
};

export const addWorkoutAddress = (workoutAddress, navigation) => {
  return (dispatch) => {
    dispatch({
      type: type.Add_Workout_Address,
      workOutAddressPlaceId: workoutAddress.place_id,
      workoutAddressName: workoutAddress.name,
    });
    navigation.goBack(null);
  };
};

export const hideSignUpSuccessDialog = () => {
  return (dispatch) => {
    dispatch({type: type.Hide_SignUp_Success_Dialog});
  };
};

export const hideSignUpErrorDialog = () => {
  return (dispatch) => {
    dispatch({type: type.Hide_SignUp_Error_Dialog});
  };
};

export const clearSignUpFieldsData = () => {
  return (dispatch) => {
    dispatch({type: type.Clear_SignUp_Fields_Data});
  };
};
