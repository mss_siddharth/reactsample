import {AsyncStorage} from 'react-native';
import {NavigationActions} from 'react-navigation';
import WebApi from '../Utilities/RestClient';
import * as type from '../ActionTypes';
import {showLoader, hideLoader} from './AppAction';

export const getUserProfile = (userData, navigation) => {
  return (dispatch) => {
    dispatch({payload: userData, type: type.Fetch_User_Data});
    WebApi.GetApiHit(
      `api/users/getProfile/${userData.id}.json`,
      null,
      userData.token
    ).then((profileResponse) => {
      if (profileResponse.success) {
        dispatch({
          profileData: profileResponse.data,
          type: type.Fetch_Profile_Success,
          unread_msg: profileResponse.data.unread_msgs,
          userData: userData,
        });
      } else {
        alert(profileResponse.data.message);
        AsyncStorage.removeItem('initialStack');
        AsyncStorage.removeItem('userData');
        navigation.dispatch(
          NavigationActions.reset({
            actions: [NavigationActions.navigate({routeName: 'AppLoginStack'})],
            index: 0,
            key: null,
          })
        );
      }
    });
  };
};

export const onLogOut = (userId, token, navigation) => (dispatch) => {
  dispatch(showLoader());
  WebApi.PostApiHitWithHeader(
    'api/users/logout.json',
    {user_id: userId},
    token
  ).then((logoutResponse) => {
    if (logoutResponse.success) {
      dispatch(hideLoader());
      AsyncStorage.removeItem('initialStack');
      AsyncStorage.removeItem('userData');
      navigation.dispatch(
        NavigationActions.reset({
          actions: [NavigationActions.navigate({routeName: 'AppLoginStack'})],
          index: 0,
          key: null,
        })
      );
    } else {
      alert(logoutResponse.data.message);
      dispatch(hideLoader());
      AsyncStorage.removeItem('initialStack');
      AsyncStorage.removeItem('userData');
      navigation.dispatch(
        NavigationActions.reset({
          actions: [NavigationActions.navigate({routeName: 'AppLoginStack'})],
          index: 0,
          key: null,
        })
      );
    }
  });
};