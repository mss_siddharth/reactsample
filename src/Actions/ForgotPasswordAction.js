import WebApi from '../Utilities/RestClient';
import * as type from '../ActionTypes';
import {
  showLoader, hideLoader,
} from './AppAction';

export const onResetPassword = (userEmail) => (dispatch) => {
  dispatch(showLoader());
  WebApi.PostApiHit('api/users/forgotpassword.json', {email: userEmail}).then((resetPasswordResponse) => {
    if (resetPasswordResponse.success) {
      dispatch({
        successMessage: resetPasswordResponse.data.result,
        type: type.Password_Reset_Success,
      });
      dispatch(hideLoader());
    } else {
      dispatch(hideLoader());
      dispatch({
        errorMessage: resetPasswordResponse.data.message,
        type: type.Password_Reset_Failure,
      });
    }
  });
};

export const hideSuccessDialog = () => (dispatch) => {
  dispatch({type: type.Hide_Success_Dialog});
};
export const hideErrorDialog = () => (dispatch) => {
  dispatch({type: type.Hide_Error_Dialog});
};
