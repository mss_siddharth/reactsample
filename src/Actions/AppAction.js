import * as type from '../ActionTypes';

export const showLoader = () => (dispatch) => {
  dispatch({type: type.Show_Loader});
};

export const hideLoader = () => (dispatch) => {
  dispatch({type: type.Hide_Loader});
};
