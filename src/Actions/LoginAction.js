import {AsyncStorage} from 'react-native';
import {NavigationActions} from 'react-navigation';
import SplashScreen from 'react-native-smart-splash-screen';
import WebApi from '../Utilities/RestClient';
import * as type from '../ActionTypes';
import {showLoader, hideLoader} from './AppAction';

export const onLogin = (userData, navigation) => (dispatch) => {
  dispatch({type: type.Clear_User_Detail_Data});
  dispatch(showLoader());
  WebApi.PostApiHit('api/users/login.json', userData).then((loginResponse) => {
    if (loginResponse.success) {
      dispatch({
        type: type.Login_Success,
        userData: loginResponse.data,
      });
      AsyncStorage.setItem('userData', JSON.stringify(loginResponse.data));
      dispatch(hideLoader());
      navigation.dispatch(
        NavigationActions.reset({
          actions: [NavigationActions.navigate({routeName: 'DrawerStack'})],
          index: 0,
          key: null,
        })
      );
    } else {
      dispatch(hideLoader());
      dispatch({
        errorMessage: loginResponse.data.message,
        type: type.Login_Failure,
      });
    }
  });
};
export const getCurrentLocation = () => (dispatch) => {
  navigator.geolocation.getCurrentPosition(
    (position) => {
      const region = {
        latitude: position.coords.latitude,
        latitudeDelta: 0.008,
        longitude: position.coords.longitude,
        longitudeDelta: 0.005,
      };

      fetchCurrentLocation(position,region,dispatch);
    },
    (error) => console.log(error.message),
    {enableHighAccuracy: true,maximumAge: 1000, timeout: 20000}
  );
  this.watchID = navigator.geolocation.watchPosition((position) => {
    const region = {
      latitude: position.coords.latitude,
      latitudeDelta: 0.008,
      longitude: position.coords.longitude,
      longitudeDelta: 0.005,
    };

    fetchCurrentLocation(position,region,dispatch);
  });
};

function fetchCurrentLocation(position,region,dispatch){
  fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?address=${
      position.coords.latitude
    },${
      position.coords.longitude
    }&key=AIzaSyCX6OQ4RL854Fv03V50pAgyyCIL-FO1b0o`
  )
    .then((response) => response.json())
    .then((responseJson) => {
      fetch(
        `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${
          position.coords.latitude
        },${
          position.coords.longitude
        }&radius=1500&type=gym&keyword=Fitness&key=AIzaSyCX6OQ4RL854Fv03V50pAgyyCIL-FO1b0o`
      )
        .then((response) => response.json())
        .then((gymResponse) => {
          dispatch({
            gymResponse,
            payload: responseJson.results[0].formatted_address,
            region,
            type: type.Current_Location,
          });
        });
    });
}
export const hideLoginErrorDialog = () => (dispatch) => {
  dispatch({type: type.Hide_Login_Error_Dialog});
};
const dismissSplashScreen = () =>
  SplashScreen.close({
    animationType: SplashScreen.animationType.scale,
    delay: 500,
    duration: 500,
  });

export const checkLoginStatus = (
  navigation,
  userData,
  notificationSenderId
) => () => {
  WebApi.GetApiHit(`api/users/loginStatus/${userData.id}.json`, null).then(
    async (user) => {
      if (user.success) {
        try {
          const stack = await AsyncStorage.getItem('initialStack');

          if (notificationSenderId === '') {
            if (stack !== null) {
              navigation.dispatch(
                NavigationActions.reset({
                  actions: [NavigationActions.navigate({routeName: stack})],
                  index: 0,
                  key: null,
                })
              );
            }
            dismissSplashScreen();
          } else {
            navigation.dispatch(
              NavigationActions.reset({
                actions: [NavigationActions.navigate({routeName: stack})],
                index: 0,
                key: null,
              })
            );
            navigation.navigate('ChatScreen', {
              chatUserId: notificationSenderId,
              fromNotification: true,
              notificationUserData: userData,
            });
            dismissSplashScreen();
          }
        } catch (error) {
          alert(error);
        }
      } else {
        alert(user.data.message);
        dismissSplashScreen();
      }
    }
  );
};
