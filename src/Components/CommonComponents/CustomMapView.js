import React from 'react';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Metrics from '../../Config/Metrics';

class CustomMapView extends React.Component {

  render() {
    const {LoginState} = this.props;

    return (
      <MapView.Animated
        ref={(map) => (this.map = map)}
        provider={PROVIDER_GOOGLE}
        style={styles.mapView}
        region={LoginState.currentRegion}
        showsUserLocation={true}
        followUserLocation={false}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    LoginState: state.LoginReducer,
  };
}

export default connect(
  mapStateToProps
)(CustomMapView);

const styles = StyleSheet.create({
  mapView: {flex: 1},
});
