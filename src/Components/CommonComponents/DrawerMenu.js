import PropTypes from 'prop-types';
import React, {Component} from 'react';
import FontIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import {LoginManager} from 'react-native-fbsdk';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import LinearGradient from 'react-native-linear-gradient';
import {Avatar} from 'react-native-elements';
import {NavigationActions} from 'react-navigation';
import {
  ScrollView,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Metrics from '../../Config/Metrics';
import {connect} from 'react-redux';
import {onLogOut} from '../../Actions/UserDetailAction';
import {language} from '../../Config/Localization';

class DrawerMenu extends Component {
  static navigationOptions = {
    header: null,
  };

  navigateToScreen(route) {
    const {navigation} = this.props;

    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });

    navigation.dispatch(navigateAction);
  }

  render() {
    const {UserDetailState,onPressLogOut,navigation} = this.props;

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.subContainer}>
            <View style={styles.drawerMenuView}>
              <Avatar
                large
                rounded
                source={
                  UserDetailState.profileData == null
                    ? require('../../Assets/user_image.png')
                    : UserDetailState.profileData.imageUrl === ''
                      ? require('../../Assets/user_image.png')
                      : {uri: UserDetailState.profileData.imageUrl}
                }
                activeOpacity={0.7}
              />
              <Text numberOfLines={1} style={styles.userNameText}>
                {UserDetailState.profileData != null
                  ? `${UserDetailState.profileData.firstname} ${UserDetailState.profileData.lastname}`
                  : ''}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.homeView}
              onPress={() => this.navigateToScreen('DrawerLocationScreen')}
            >
              <View style={styles.inputStyle}>
                <EntypoIcon name="home" size={25} color="grey" />
                <View style={styles.homeContainer}>
                  <Text style={styles.homeText}>{'Home'}</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.homeView}
              onPress={() => this.navigateToScreen('ProfileScreen')}
            >
              <View style={styles.inputStyle}>
                <FontIcon name="user-circle-o" size={25} color="grey" />
                <View style={styles.homeContainer}>
                  <Text style={styles.homeText}>{'Profile'}</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.homeView}
              onPress={() => this.navigateToScreen('InboxScreen')}
            >
              <View style={styles.inputStyle}>
                <EntypoIcon name="message" size={25} color="grey" />
                <View style={styles.homeContainer}>
                  <Text style={styles.homeText}>{'Inbox'}</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.homeView}
              onPress={() => this.navigateToScreen('HelpCenterScreen')}
            >
              <View style={styles.inputStyle}>
                <IonIcon name="ios-help-circle" size={25} color="grey" />
                <View style={styles.homeContainer}>
                  <Text style={styles.homeText}>{'Help Center'}</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.homeView}
              onPress={() => this.navigateToScreen('NotificationScreen')}
            >
              <View style={styles.inputStyle}>
                <IonIcon name="md-send" size={25} color="grey" />
                <View style={styles.homeContainer}>
                  <Text style={styles.homeText}>{'Notifications'}</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <TouchableOpacity
          style={styles.touchableStyle}
          onPress={() => {
            LoginManager.logOut();
            onPressLogOut(
              UserDetailState.userData.id,
              UserDetailState.userData.token,
              navigation
            );
          }}
        >
          <LinearGradient
            colors={['#eda374', '#cb6133', '#cb6133']}
            style={styles.linearGradient}
          >
            <View style={styles.subContainerView}>
              <Text style={styles.buttonText}>{language.en.logOut}</Text>
            </View>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
    marginRight: 20,
  },
  container: {
    flex: 1,
    height: Metrics.DEVICE_HEIGHT,
  },
  drawerMenuView: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginBottom: 20,
    marginTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  emptyView: {height: 20, width: 20},
  frameView: {
    height: Metrics.DEVICE_HEIGHT,
    position: 'absolute',
    width: Metrics.DEVICE_WIDTH,
  },
  homeContainer: {
    alignItems: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap',
    marginLeft: 30,
  },
  homeText: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
  },
  homeView: {
    flex: 1,
    height: 50,
  },
  inputStyle: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  linearGradient: {
    flex: 1,
    height: 45,
  },
  messageCountBg: {
    alignItems: 'center',
    height: 30,
    justifyContent: 'center',
    marginLeft: 20,
    width: 30,
  },
  subContainer: {flex: 1, flexDirection: 'column'},
  subContainerView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 8,
  },
  touchableStyle: {
    bottom: 0,
    height: 45,
    position:'absolute',
    width: Metrics.DEVICE_WIDTH * 0.7,
  },
  unreadMessageCountText: {
    color: 'white',
    fontSize: 12,
    height: 30,
    paddingTop: 7,
    textAlign: 'center',
    width: 30,
  },
  userNameText: {
    color: 'black',
    flex: 1,
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 20,
    textAlign: 'left',
  },
});

DrawerMenu.propTypes = {
  navigation: PropTypes.object,
};
function mapStateToProps(state) {
  return {
    LoginState: state.LoginReducer,
    UserDetailState: state.UserDetailReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onPressLogOut: (userId, token, navigation) =>
      dispatch(onLogOut(userId, token, navigation)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerMenu);
