import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Image,
} from 'react-native';
import Metrics from '../../Config/Metrics';
import Icon from 'react-native-vector-icons/Entypo';
import {Color} from '../../Constants/Colors';
import {connect} from 'react-redux';
class DrawerNavBar extends Component {
  render() {
    const {navigation,LoginState,title} = this.props;

    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.drawerNavBar}
          onPress={() => {
            navigation.state.index === 0
              ? navigation.navigate('DrawerOpen')
              : navigation.navigate('DrawerClose');
          }}
        >
          <Icon size={30} color="white" name="menu" />
          {LoginState.notificationBadgeCount > 0 && (
            <View style={styles.dotStyle} />
          )}
        </TouchableOpacity>
        <View style={styles.navBarContainer}>
          <Image
            style={styles.appIcon}
            source={require('../../Assets/spotter_icon.png')}
          />
          <Text style={styles.titleText}>{title}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  addButton: {marginRight: 10, padding: 5},
  appIcon: {height: 35, width: 35},
  container: {
    alignItems: 'center',
    backgroundColor: Color.colorBlack,
    flexDirection: 'row',
    height: Platform.OS === 'android' ? 55 : 65,
    justifyContent: 'space-between',
    paddingTop: Platform.OS === 'android' ? 0 : 25,
    width: Metrics.DEVICE_WIDTH,
  },
  dotStyle: {
    backgroundColor: Color.colorPrimaryDark,
    borderRadius: 5,
    height: 10,
    position: 'absolute',
    right: 5,
    top: 10,
    width: 10,
  },
  drawerNavBar: {flexWrap: 'wrap', padding: 5},
  navBarContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: Platform.OS === 'android' ? 0 : 5,
    paddingRight: 35,
  },
  titleText: {
    color: '#4A8EDF',
    fontSize: 19,
    fontWeight: 'bold',
    marginLeft: 10,
  },
});

function mapStateToProps(state) {
  return {
    ChatState: state.ChatReducer,
    LoginState: state.LoginReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerNavBar);
