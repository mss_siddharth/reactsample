import * as type from '../ActionTypes';
const initialState = {
  profileData: null,
  userData: null,
};

export default function UserDetailReducer(state = initialState, action) {
  switch (action.type) {
    case type.Fetch_Profile_Success:
      return {
        ...state,
        profileData: action.profileData,
        userData: action.userData,
      };
    case type.Clear_User_Detail_Data:
      return {
        profileData: null,
        userData: null,
      };
    default:
      return state;
  }
}
