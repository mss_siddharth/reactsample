import * as type from '../ActionTypes';
const initialState = {
  errorMessage: '',
  showApiLoader: false,
  showSignUpErrorDialog: false,
  showSignUpSuccessDialog: false,
  signUpResponse: null,
};

export default function SignUpReducer(state = initialState, action) {
  switch (action.type) {
    case type.SignUp_Success:
      return {
        ...state,
        showSignUpSuccessDialog: true,
        signUpResponse: action.signUpResponse,
      };
    case type.SignUp_Failure:
      return {
        ...state,
        errorMessage: action.errorMessage,
        showSignUpErrorDialog: true,
      };
    case type.Hide_SignUp_Success_Dialog:
      return {
        ...state,
        showSignUpSuccessDialog: false,
      };
    case type.Hide_SignUp_Error_Dialog:
      return {
        ...state,
        showSignUpErrorDialog: false,
      };
    case type.Clear_SignUp_Fields_Data:
      return {
        ...state,
        errorMessage: '',
        showApiLoader: false,
        showSignUpErrorDialog: false,
        showSignUpSuccessDialog: false,
        signUpResponse: null,
      };
    default:
      return state;
  }
}
