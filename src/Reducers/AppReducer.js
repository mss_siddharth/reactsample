import * as type from '../ActionTypes';
const initialState = {
  showLoading: false,
  userData: null,
};

export default function AppReducer(state = initialState, action) {
  switch (action.type) {
    case type.Show_Loader:
      return {
        ...state,
        showLoading: true,
      };
    case type.Fetch_User_Data:
      return {
        ...state,
        userData: action.payload,
      };
    case type.Hide_Loader:
      return {
        ...state,
        showLoading: false,
      };
    default:
      return state;
  }
}
