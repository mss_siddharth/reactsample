import * as type from '../ActionTypes';
const initialState = {
  currentAddress: 'Location',
  currentRegion: null,
  errorMessage: '',
  showErrorDialog: false,
  userData: null,
};

export default function LoginReducer(state = initialState, action) {
  switch (action.type) {
    case type.Login_Success:
      return {
        ...state,
        userData: action.userData,
      };
    case type.Current_Location:
      return {
        ...state,
        currentAddress: action.payload,
        currentRegion: action.region,
      };
    case type.Login_Failure:
      return {
        ...state,
        errorMessage: action.errorMessage,
        showErrorDialog: true,
      };
    case type.Hide_Login_Error_Dialog:
      return {
        ...state,
        showErrorDialog: false,
      };
    default:
      return state;
  }
}
