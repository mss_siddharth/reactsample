import {combineReducers} from 'redux';
import LoginReducer from './LoginReducer';
import SignUpReducer from './SignUpReducer';
import AppReducer from './AppReducer';
import UserDetailReducer from './UserDetailReducer';
import ForgotPasswordReducer from './ForgotPasswordReducer';
const rootReducer = combineReducers({
  AppReducer,
  ForgotPasswordReducer,
  LoginReducer,
  SignUpReducer,
  UserDetailReducer,
});

export default rootReducer;
