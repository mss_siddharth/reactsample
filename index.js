import React from 'react';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';
import configureStore from './src/Config/ConfigureStore';
import {name as AppName} from './app.json';
import App from './App';

const store = configureStore();

class Application extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <App/>
      </Provider>
    );
  }
}
AppRegistry.registerComponent(AppName, () => Application);
