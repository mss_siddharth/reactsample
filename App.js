import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View, ActivityIndicator,AsyncStorage} from 'react-native';
import Navigator from  './src/Config/Navigator';
import {Color} from './src/Constants/Colors';
import Metrics from './src/Utilities/Metrics';
const styles = StyleSheet.create({
  appIndicator: {flex: 1},
  container: {flex: 1},
  indicatorView: {
    alignItems: 'center',
    height: Metrics.DEVICE_HEIGHT,
    justifyContent: 'center',
    position: 'absolute',
    width: Metrics.DEVICE_WIDTH,
  },
});

class App extends Component {
  componentDidMount(){
    console.disableYellowBox = true;
  }

  renderLoader = () => {
    if (this.props.AppReducerState.showLoading) {
      return (
        <View style={styles.indicatorView}>
          <ActivityIndicator
            style={styles.appIndicator}
            color={Color.colorPrimary}
            size="small"
            animating={this.props.AppReducerState.showLoading}
          />
        </View>
      );
    }
    return null;
  };

  render() {
    return (
      <View style={styles.container}>
        <Navigator />
        {this.renderLoader()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {AppReducerState: state.AppReducer,ChatState: state.ChatReducer};
}

export default connect(mapStateToProps)(App);
